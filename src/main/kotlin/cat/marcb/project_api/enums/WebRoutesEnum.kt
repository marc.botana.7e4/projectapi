package com.example.enums

enum class WebRoutesEnum(val route: String, val title: String) {
    all("all","All Films"),
    new("new","Add New Film"),
    detail("detail/{id?}","Film Details"),
    about("about","About Us"),
}