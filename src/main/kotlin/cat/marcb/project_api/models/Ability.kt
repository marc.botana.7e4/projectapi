package cat.marcb.project_api.models

import kotlinx.serialization.Serializable



@Serializable
data class Ability(
    var nameAb: String,
    var descAb: String,
    var abIcon: String = ""
)


