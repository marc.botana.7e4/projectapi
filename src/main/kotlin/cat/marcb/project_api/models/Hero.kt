package cat.marcb.project_api.models

import kotlinx.serialization.Serializable

var globalId = 0

@Serializable
data class Hero(
    var name: String,
    var rol: String,
    var desc: String,
    var abilities: Ability,
    var image: String = ""
) {
    var id: String

    init {
        globalId += 1
        id = globalId.toString()
    }
}

val heroStorage = mutableListOf<Hero>()
const val pathImages = "/static/images/movies/"