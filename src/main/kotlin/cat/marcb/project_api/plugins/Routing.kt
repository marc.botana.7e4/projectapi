package com.example.plugins

import com.example.routes.api.filmRouting
import com.example.routes.web.webRouting
import io.ktor.server.routing.*
import io.ktor.server.application.*
import io.ktor.server.http.content.*

fun Application.configureRouting() {
    routing {
        resource("/", "index.html")
        resource("*", "index.html")

        static("static") {
            resources("static")
        }
        filmRouting()
        webRouting()
    }
}
