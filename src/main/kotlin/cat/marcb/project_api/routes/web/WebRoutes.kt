package com.example.routes.web

import com.example.enums.WebRoutesEnum
import com.example.models.filmStorage
import com.example.templates.LayoutTemplate
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.webRouting() {

    route("/") {
        get("/") {
            call.respondRedirect("/all")
        }

        //ALL FILMS
        get(WebRoutesEnum.all.route) {

            call.respondHtmlTemplate(LayoutTemplate()) {
                this.content = WebRoutesEnum.all.route
                this.sTitle = WebRoutesEnum.all.title
            }
        }

        //ADD NEW FILM
        get(WebRoutesEnum.new.route) {
            call.respondHtmlTemplate(LayoutTemplate()) {
                this.content = WebRoutesEnum.new.route
                this.sTitle = WebRoutesEnum.new.title
            }
        }

        //FILM DETAILS
        get(WebRoutesEnum.detail.route) {

            val exmovieId = call.parameters["id"] ?: return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            call.respondHtmlTemplate(LayoutTemplate()) {
                this.moveId = exmovieId
                this.content = WebRoutesEnum.detail.route
                this.sTitle = WebRoutesEnum.detail.title

            }
        }

        //ABOUT US
        get(WebRoutesEnum.about.route) {
            call.respondHtmlTemplate(LayoutTemplate()) {
                this.content = WebRoutesEnum.about.route
                this.sTitle = WebRoutesEnum.about.title
            }
        }

        //DELETE FILM
        get("delete/id/{id?}") {
            val exmovieId = call.parameters["id"] ?: return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            filmStorage.removeIf { it.id == exmovieId }
            call.respondRedirect("/all")
        }

        //ABOUT US
        get(WebRoutesEnum.about.route) {
            call.respondHtmlTemplate(LayoutTemplate()) {
                this.content = WebRoutesEnum.about.route
                this.sTitle = WebRoutesEnum.about.title
            }
        }
    }
}