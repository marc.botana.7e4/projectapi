package com.example.routes.api

import cat.marcb.project_api.models.Ability
import cat.marcb.project_api.models.Hero
import cat.marcb.project_api.models.heroStorage
import cat.marcb.project_api.models.pathImages
import com.example.enums.WebRoutesEnum
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.io.File

fun Route.filmRouting() {
    route("api") {

        //GET ALL FILMS
        get("all") {
            if (heroStorage.isNotEmpty()) {
                call.respond(heroStorage)
            }
        }

        //GET FILM
        get("id/{id?}") {
            val id = call.parameters["id"] ?: return@get call.respondText(
                "Missing id!",
                status = HttpStatusCode.BadRequest
            )
            val film =
                heroStorage.find { it.id == id } ?: return@get call.respondText(
                    "No film with id $id!",
                    status = HttpStatusCode.NotFound
                )
            call.respond(film)
        }

        //POST FILM
        post("add") {
            val data = call.receiveMultipart()
            val objAb = Ability("", "", "")
            val objHero = Hero("","","",objAb, "")

            data.forEachPart { part ->
                when (part) {
                    is PartData.FormItem -> {
                        when (part.name){
                            "nameAb" -> objAb.nameAb = part.value
                            "descAb" -> objAb.descAb = part.value
                            "abIcon" -> objAb.abIcon = part.value
                            "name" -> objHero.name = part.value
                            "rol" -> objHero.rol = part.value
                            "desc" -> objHero.desc = part.value
                            "image" -> objHero.image = part.value
                        }
                    }

                    //Segona part del when
                    is PartData.FileItem -> {
                        objHero.image = (objHero.name + "." + (part.originalFileName as String).substringAfterLast(".")).replace(" ", "_")
                        var fileBytes = part.streamProvider().readBytes()
                        File(File(".").getCanonicalPath()+"/build/resources/main"+ pathImages + objHero.image).writeBytes(fileBytes)
                    }
                    else -> {
                        println("No se ha podido guardar la imagen")
                    }
                }}
            heroStorage.add(objHero)
            println(heroStorage)
            call.respondRedirect("../${WebRoutesEnum.detail}/${objHero.id.toString()}")
            call.respondText("Film stored correctly and \"${objHero.image} is uploaded to '${pathImages+objHero.image}'\"", status = HttpStatusCode.Created)
        }

        //UPDATE FILM
        put("update/id/{id?}") {
            val id = call.parameters["id"] ?: return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val customerOld =
                heroStorage.find { it.id == id } ?: return@put call.respondText(
                    "No film with id $id!",
                    status = HttpStatusCode.NotFound
                )
            val pos = heroStorage.indexOf(customerOld)
            val filmNew = call.receive<Hero>()
            heroStorage[pos] = filmNew
            call.respondText("Film updated correctly!", status = HttpStatusCode.Created)
        }

        //DELETE FILM
        delete("delete/id/{id?}") {
            val id = call.parameters["id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
            if (heroStorage.removeIf { it.id == id }) {
                call.respondText("Film removed correctly!", status = HttpStatusCode.Accepted)
            } else {
                call.respondText("Not Found!", status = HttpStatusCode.NotFound)
            }
        }

        //GET IMAGE
        get("uploads/{imageName}") {
            val imageName = call.parameters["imageName"]
            var file = File("./uploads/$imageName")
            if(file.exists()){
                call.respondFile(File("./uploads/$imageName"))
            }
            else{
                call.respondText("Image not found", status = HttpStatusCode.NotFound)
            }
        }
    }
}